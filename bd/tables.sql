create table TB_AREA(
id_area int auto_increment,
name varchar(30) not null,
primary key (id_area),
constraint uq_name_area unique (name)
);

create table TB_USER(
id_user char(5) not null,
name varchar(30) not null,
lastname varchar(30) not null,
motherlastname varchar(30),
username varchar(20) not null,
password text not null,
phone char(9) not null,
status int not null default 0,
role char(1) not null,
id_area int not null,
primary key (id_user),
foreign key (id_area) references TB_AREA (id_area),
constraint uq_id_user unique (id_user)
);

create table TB_INTERACTION(
id_interaction int auto_increment,
user_to_register char(5) not null,
user_registered char(5) not null,
date timestamp default current_timestamp,
primary key (id_interaction)
);