insert into TB_AREA value (null,'Integration');
insert into TB_AREA value (null,'Agile');
insert into TB_AREA value (null,'Cloud');

insert into TB_USER (id_user, name, lastname, motherlastname, username, password, phone, role, id_area)
values 
('U1000','Raul','Penilla','Navarro','rpenilla','admin', '999771821', 'A',1),
('U1001','Royer','Robles','Vega','rrobles','admin', '999888777', 'A',1),
('U1002','Jazmin','Guerrero', null,'jguerrero','user', '999111222', 'U',2),
('U1003','Omar','Camino', null,'ocamino','user', '999333444', 'U',3);
