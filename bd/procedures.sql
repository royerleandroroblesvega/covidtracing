delimiter $$
create procedure sp_get_user_by_id(idUsuario char(5))
begin
  select 
  U.id_user,
  U.name,
  U.lastname,
  U.motherlastname,
  U.username,
  U.password,
  U.phone,
  U.status,
  U.role,
  A.name as name_area
  from TB_USER as U 
  inner join TB_AREA as A 
  on U.id_area = A.id_area
  where U.id_user = idUsuario;
end$$
delimiter ;


delimiter $$
create procedure sp_validate_user(
in v_username varchar(20),
in v_password text,
out id_user int,
out return_code int
)
begin
    set return_code = 0;
    set @idUsuario = null;
    
	if exists (
		select U.id_user
		from TB_USER as U 
		where U.username = v_username 
		and U.password = v_password) then
		
		select U.id_user
		into @idUsuario
		from TB_USER as U 
		where U.username = v_username 
		and U.password = v_password;
		set return_code = 1;
		select @idUsuario as id_user, return_code;
        
    else
		select @idUsuario as id_user, return_code;  
    end if;
end$$
delimiter ;

delimiter $$
create procedure sp_insert_interaction(
idUsuarioA char(5), 
idUsuarioB char(5),
out return_code int)
begin
	set return_code = 0;

	if exists (
		select U.id_USER 
        from TB_USER as U 
        where U.id_user = idUsuarioA) then
		
        if exists (
			select U.id_USER 
			from TB_USER as U 
			where U.id_user = idUsuarioB) then
        
			if (idUsuarioA != idUsuarioB) then
				set return_code = 1;
				insert into TB_INTERACTION values (null, idUsuarioA, idUsuarioB, CURRENT_TIMESTAMP());
			end if;
		end if;
	end if;
	select return_code;
end$$
delimiter ;

delimiter $$
create procedure sp_get_interactions_by_user(
idUsuario char(5), 
out return_code int)
begin
	if exists (
		select U.id_USER 
        from TB_USER as U 
        where U.id_user = idUsuario) then
        
		select distinct
        I.id_interaction,
        I.user_registered,
        I.date
        from TB_INTERACTION as I
        where I.user_to_register = idUsuario
        and I.date >= (current_timestamp() - interval 7 day)
        order by I.date desc;
    
    else
		set return_code = 0;
		select return_code;
	end if;
end$$
delimiter ;