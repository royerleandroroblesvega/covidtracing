#from flask import Flask
from flask import Flask, jsonify , request, render_template
import mysql.connector
#from flask_mysqldb import MySQL
from flaskext.mysql import MySQL
#import dbOracle as db
import dbMysql as db

#app = Flask(__name__)
app = Flask(__name__,template_folder='templates')
mysql = MySQL()

"""
app.config['MYSQL_USER'] = 'utestdbcovid'
app.config['MYSQL_PASSWORD'] = 'ptestdbcovid'
app.config['MYSQL_HOST'] = 'a2plcpnl0788.prod.iad2.secureserver.net'
app.config['MYSQL_DB'] = 'testdbcovid'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'


MYSQL_DATABASE_HOST	default is ‘localhost’
MYSQL_DATABASE_PORT	default is 3306
MYSQL_DATABASE_USER	default is None
MYSQL_DATABASE_PASSWORD	default is None
MYSQL_DATABASE_DB	default is None
MYSQL_DATABASE_CHARSET	default is ‘utf-8’
"""

app.config['MYSQL_DATABASE_USER'] = 'utestdbcovid'
app.config['MYSQL_DATABASE_PASSWORD'] = 'ptestdbcovid'
app.config['MYSQL_DATABASE_DB'] = 'testdbcovid'
app.config['MYSQL_DATABASE_HOST'] = 'a2plcpnl0788.prod.iad2.secureserver.net'

#mysql = MySQL()
mysql.init_app(app)

conn = mysql.connect()

"""
mydb = mysql.connector.connect(
  host="a2plcpnl0788.prod.iad2.secureserver.net",
  user="utestdbcovid",
  password="ptestdbcovid",
  database="testdbcovid"
)
"""

@app.route('/')
def hello():
    return "Hola Mundo Covid!"


@app.route('/index')
def index():
   return render_template('index.html')

@app.route('/<name>')
def hello_name(name):
    return "Hello {}!".format(name)



@app.route('/user', methods=['GET'])
def routeListUsers():

    result = {
        'id_user': 1000 ,
        'name': 'Royer Leandro',
        'lastname': 'Robles',
        'motherlastname': 'Vega',
        'username': 'royer.robles',
        'password': 'royer.robles',
        'area': 'IT'
    }

    return jsonify({'userInformation': result})

@app.route('/user/v1/<userId>', methods=['GET'])
def returnListUsers(userId):

    cur = conn.cursor()
    """
    sql = "call sp_get_user_by_id('{userId}')".format(userId=userId)
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    """
    sql = "call sp_get_user_by_id('{userId}');".format(userId=userId)
    cur.execute(sql)
    myresult = cur.fetchall()
    return jsonify({'result': myresult})

"""
#rs = db.sp_get_user_by_id(userId)
#return jsonify({'result': rs})

"""

@app.route('/album/index')
def album():
   return render_template('album/index.html')

@app.route('/offcanvas/index')
def offcanvas():
   return render_template('offcanvas/index.html')

@app.route('/sign-in/index')
def signIn():
   return render_template('sign-in/index.html')

if __name__ == '__main__':
    app.run()
